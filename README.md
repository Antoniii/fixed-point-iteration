// java -version  
// curl -s "https://get.sdkman.io" | bash  
// source "$HOME/.sdkman/bin/sdkman-init.sh"  
// sdk version  
// sdk install kotlin  
// kotlin -version  

// kotlinc hoi.kt -include-runtime -d hoi.jar && java -jar hoi.jar  


// import kotlin.math.*  

![](https://gitlab.com/Antoniii/fixed-point-iteration/-/raw/main/Kotlin.png)

![](https://gitlab.com/Antoniii/fixed-point-iteration/-/raw/main/kotlinc.PNG)

![](https://gitlab.com/Antoniii/fixed-point-iteration/-/raw/main/Снимок.PNG)


## 5*x - 8*ln(x) = 8, eps = 0.001  

![](https://gitlab.com/Antoniii/fixed-point-iteration/-/raw/main/Figure_1.png)



## Sources

* [Основы Kotlin. Простые функции](https://www.fandroid.info/1-osnovy-kotlin-prostye-funktsii/2/)
* [kotlin-stdlib / kotlin.math / abs](https://kotlinlang.org/api/latest/jvm/stdlib/kotlin.math/abs.html)
* [Использование ln() функция](https://www.techiedelight.com/ru/calculate-log-base-2-of-a-number-in-kotlin/)
* [Запись в файл в Kotlin](https://www.techiedelight.com/ru/write-to-file-kotlin/)
* [Kotlin Programming By Example Build real-world Android and web applications the Kotlin way. lyanu Adelekan](https://djvu.online/file/zqh51lRYp5Fwf)
* [МЕТОДЫ ЧИСЛЕННОГО АНАЛИЗА](https://lib.brsu.by/sites/default/files/books/МЧА.pdf)
