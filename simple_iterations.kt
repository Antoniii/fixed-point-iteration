// kotlinc simple_iterations.kt -include-runtime -d si.jar && java -jar si.jar

import java.io.File
import kotlin.math.ln
import kotlin.math.abs

fun f(x: Double): Double {
	return 1.6*(ln(x)+1)
}

fun simple_iterations(){
	
	val eps: Double = 0.001

	var c: Double = 3.3
	
	var x: Double = f(c)

	while(abs(x - c) > eps) {
		c = x
		x = f(c)
	}

	println(x)

	val file = File("result.txt")
    val text = x.toString()
 
    file.writeText(text, Charsets.UTF_8)
}

fun main () {
	simple_iterations()
}