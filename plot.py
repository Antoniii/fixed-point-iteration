import matplotlib.pyplot as plt
import numpy as np

# Создаём экземпляр класса figure и добавляем к Figure область Axes
fig, ax = plt.subplots()
# Добавим заголовок графика
ax.set_title('График функции y = 5x - 8(ln(x)+1)')
# Название оси X:
ax.set_xlabel('x')
# Название оси Y:
ax.set_ylabel('y')
# Начало и конец изменения значения X, разбитое на 100 точек
x = np.linspace(0.01, 10, 100) # X от и до 
# Построение прямой
y = 5*x - 8*(np.log(x)+1) # корни примерно: 0.5 и 3.65
# Вывод графика
ax.plot(x, y)
plt.grid()
plt.show()